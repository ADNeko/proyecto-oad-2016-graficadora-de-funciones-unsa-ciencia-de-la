/********************************************************************************
** Form generated from reading UI file 'ventana3d.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VENTANA3D_H
#define UI_VENTANA3D_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ventana3d
{
public:
    QWidget *centralwidget;
    QLineEdit *text_funcion;
    QPushButton *graficar;
    QPushButton *regresar2d;
    QPushButton *limpiar;
    QLineEdit *text_funcion_2;
    QPushButton *graficar_2;
    QPushButton *limpiar_2;
    QLineEdit *text_funcion_3;
    QPushButton *graficar_3;
    QPushButton *limpiar_3;
    QPushButton *salir;
    QFrame *line;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_4;
    QPushButton *guardar;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *namepng;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ventana3d)
    {
        if (ventana3d->objectName().isEmpty())
            ventana3d->setObjectName(QStringLiteral("ventana3d"));
        ventana3d->resize(1501, 802);
        ventana3d->setMinimumSize(QSize(800, 800));
        centralwidget = new QWidget(ventana3d);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        text_funcion = new QLineEdit(centralwidget);
        text_funcion->setObjectName(QStringLiteral("text_funcion"));
        text_funcion->setGeometry(QRect(120, 140, 181, 31));
        graficar = new QPushButton(centralwidget);
        graficar->setObjectName(QStringLiteral("graficar"));
        graficar->setGeometry(QRect(320, 140, 91, 31));
        QFont font;
        font.setFamily(QStringLiteral("OpineHeavy"));
        graficar->setFont(font);
        regresar2d = new QPushButton(centralwidget);
        regresar2d->setObjectName(QStringLiteral("regresar2d"));
        regresar2d->setGeometry(QRect(210, 490, 111, 41));
        regresar2d->setFont(font);
        limpiar = new QPushButton(centralwidget);
        limpiar->setObjectName(QStringLiteral("limpiar"));
        limpiar->setGeometry(QRect(430, 140, 81, 31));
        limpiar->setFont(font);
        text_funcion_2 = new QLineEdit(centralwidget);
        text_funcion_2->setObjectName(QStringLiteral("text_funcion_2"));
        text_funcion_2->setGeometry(QRect(120, 210, 181, 31));
        graficar_2 = new QPushButton(centralwidget);
        graficar_2->setObjectName(QStringLiteral("graficar_2"));
        graficar_2->setGeometry(QRect(320, 210, 91, 31));
        graficar_2->setFont(font);
        limpiar_2 = new QPushButton(centralwidget);
        limpiar_2->setObjectName(QStringLiteral("limpiar_2"));
        limpiar_2->setGeometry(QRect(430, 210, 81, 31));
        limpiar_2->setFont(font);
        text_funcion_3 = new QLineEdit(centralwidget);
        text_funcion_3->setObjectName(QStringLiteral("text_funcion_3"));
        text_funcion_3->setGeometry(QRect(120, 280, 181, 31));
        graficar_3 = new QPushButton(centralwidget);
        graficar_3->setObjectName(QStringLiteral("graficar_3"));
        graficar_3->setGeometry(QRect(320, 280, 91, 31));
        graficar_3->setFont(font);
        limpiar_3 = new QPushButton(centralwidget);
        limpiar_3->setObjectName(QStringLiteral("limpiar_3"));
        limpiar_3->setGeometry(QRect(430, 280, 81, 31));
        limpiar_3->setFont(font);
        salir = new QPushButton(centralwidget);
        salir->setObjectName(QStringLiteral("salir"));
        salir->setGeometry(QRect(390, 490, 111, 41));
        salir->setFont(font);
        line = new QFrame(centralwidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(520, 100, 20, 451));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(50, 90, 481, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(centralwidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(40, 100, 20, 441));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(centralwidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(50, 540, 481, 16));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        guardar = new QPushButton(centralwidget);
        guardar->setObjectName(QStringLiteral("guardar"));
        guardar->setGeometry(QRect(330, 380, 111, 41));
        guardar->setFont(font);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 50, 351, 20));
        QFont font1;
        font1.setFamily(QStringLiteral("Showcard Gothic"));
        font1.setPointSize(16);
        label->setFont(font1);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 150, 47, 13));
        QFont font2;
        font2.setFamily(QStringLiteral("November"));
        font2.setPointSize(10);
        label_2->setFont(font2);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(60, 220, 47, 13));
        label_3->setFont(font2);
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(60, 290, 47, 13));
        label_4->setFont(font2);
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(910, 20, 351, 20));
        label_5->setFont(font1);
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(60, 350, 61, 16));
        label_6->setFont(font2);
        namepng = new QLineEdit(centralwidget);
        namepng->setObjectName(QStringLiteral("namepng"));
        namepng->setGeometry(QRect(130, 340, 311, 31));
        ventana3d->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ventana3d);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1501, 21));
        ventana3d->setMenuBar(menubar);
        statusbar = new QStatusBar(ventana3d);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        ventana3d->setStatusBar(statusbar);

        retranslateUi(ventana3d);

        QMetaObject::connectSlotsByName(ventana3d);
    } // setupUi

    void retranslateUi(QMainWindow *ventana3d)
    {
        ventana3d->setWindowTitle(QApplication::translate("ventana3d", "GRAFICADORA DE FUNCIONES  ", 0));
        graficar->setText(QApplication::translate("ventana3d", "Graficar", 0));
        regresar2d->setText(QApplication::translate("ventana3d", "regresar 2d", 0));
        limpiar->setText(QApplication::translate("ventana3d", "limpiar", 0));
        graficar_2->setText(QApplication::translate("ventana3d", "Graficar", 0));
        limpiar_2->setText(QApplication::translate("ventana3d", "limpiar", 0));
        graficar_3->setText(QApplication::translate("ventana3d", "Graficar", 0));
        limpiar_3->setText(QApplication::translate("ventana3d", "limpiar", 0));
        salir->setText(QApplication::translate("ventana3d", "Salir", 0));
        guardar->setText(QApplication::translate("ventana3d", "Guardar", 0));
        label->setText(QApplication::translate("ventana3d", "PANEL DE CONTROL FUNCIONES 3D", 0));
        label_2->setText(QApplication::translate("ventana3d", "F(x,y) =", 0));
        label_3->setText(QApplication::translate("ventana3d", "F(x,y) =", 0));
        label_4->setText(QApplication::translate("ventana3d", "F(x,y) =", 0));
        label_5->setText(QApplication::translate("ventana3d", "GRAFICA DE LA FUNCI\303\223N", 0));
        label_6->setText(QApplication::translate("ventana3d", "Nombre", 0));
    } // retranslateUi

};

namespace Ui {
    class ventana3d: public Ui_ventana3d {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VENTANA3D_H
