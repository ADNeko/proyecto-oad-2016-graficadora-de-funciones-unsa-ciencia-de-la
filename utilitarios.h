#ifndef UTILITARIOS_H_INCLUDED
#define UTILITARIOS_H_INCLUDED
#include "include.h"

namespace utilitarios
{

 inline vector_str gramatica(){
    vector_str gram;
    gram.push_back("+");
	gram.push_back("-");
	gram.push_back("*");
	gram.push_back("/");
	gram.push_back("%");
	gram.push_back("^");
	gram.push_back("(");
	gram.push_back(")");
	return gram;
 }
 inline str_natural tabla_prioriades(){
    str_natural Tabla;
	Tabla["+"]=0;
	Tabla["-"]=1;
	Tabla["/"]=2;
	Tabla["*"]=2;
	Tabla["%"]=2;
	Tabla["("]=0;
	Tabla[")"]=0;
	Tabla["^"]=3;
	Tabla["?"]=10;
	Tabla["sin"]=11;
	Tabla["cos"]=11;
	return Tabla;
 }
 inline vector_str operadores(){
    vector_str Simbols;
	Simbols.push_back("+");
	Simbols.push_back("-");
	Simbols.push_back("*");
	Simbols.push_back("/");
	Simbols.push_back("%");
	Simbols.push_back("^");
	Simbols.push_back("(");
	Simbols.push_back(")");
	Simbols.push_back("sin");
	Simbols.push_back("cos");
	return Simbols;
 }
}


#endif // UTILITARIOS_H_INCLUDED
