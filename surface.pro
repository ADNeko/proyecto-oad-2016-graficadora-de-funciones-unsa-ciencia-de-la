SOURCES += main.cpp \
           surfacegraph.cpp \
    ventana3d.cpp \
    mainwindow.cpp \
    operacion.cpp \
    parseador.cpp \
    qcustomplot.cpp \
    tabulador.cpp

HEADERS += surfacegraph.h \
    ventana3d.h \
    include.h \
    mainwindow.h \
    operacion.h \
    operaciones.h \
    parseador.h \
    qcustomplot.h \
    tabulador.h \
    utilitarios.h

QT += widgets
QT += datavisualization
QT       += core gui
CONFIG   += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport
FORMS += \
    ventana3d.ui \
    mainwindow.ui

RESOURCES += \
    images.qrc
