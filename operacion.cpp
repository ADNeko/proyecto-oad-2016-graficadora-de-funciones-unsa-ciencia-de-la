#include "operacion.h"

Operacion::Operacion()
{
    Tabla_operaciones["+"]=&Operaciones::adicion;
	Tabla_operaciones["-"]=&Operaciones::sustraccion;
	Tabla_operaciones["/"]=&Operaciones::division;
	Tabla_operaciones["*"]=&Operaciones::multiplicacion;
    Tabla_operaciones["^"]=&Operaciones::potencia;
    Tabla_operaciones["sin"]=&Operaciones::seno;
    Tabla_operaciones["cos"]=&Operaciones::coseno;

    lectura_operadores["+"]=izquierda_derecha;
	lectura_operadores["-"]=izquierda_derecha;
	lectura_operadores["/"]=izquierda_derecha;
	lectura_operadores["*"]=izquierda_derecha;
    lectura_operadores["^"]=derecha_izquierda;
    lectura_operadores["sin"]=trigonometrica;
    lectura_operadores["cos"]=trigonometrica;

}


Operacion::~Operacion()
{
    //dtor
}
void Operacion::set_signo(const _str& signo){
    this->signo=signo;
    m_ptr_funcion = Tabla_operaciones[signo];
	existe_operacion= m_ptr_funcion != nullptr;
    operacion_lectura = lectura_operadores[this->signo];
}
void Operacion::evaluar(value& a , value& b){

    if(operacion_lectura==izquierda_derecha){
         value resultado=(m_ptr_funcion(b,a));
         valor = Operaciones::to_str(resultado);
    }
   else if(operacion_lectura==derecha_izquierda){
        value resultado=(m_ptr_funcion(b,a));
        valor = Operaciones::to_str(resultado);
   }
   else{
       value resultado=(m_ptr_funcion(a,b));
       valor = Operaciones::to_str(resultado);
   }
}


