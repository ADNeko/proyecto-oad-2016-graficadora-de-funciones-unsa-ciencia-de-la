#include "tabulador.h"

tabulador::tabulador()
{
    _operacion = new Operacion;
    _parseador = new parseador;
}
tabulador::tabulador(value x_negativo,value x_positivo)
          :x_negativo(x_negativo),x_positivo(x_positivo)
{
    _operacion = new Operacion;
    _parseador = new parseador;
}
tabulador::~tabulador()
{
    delete _operacion;
    delete _parseador;
}

_str  tabulador::evaluar(cola_str Flujo){

    stack_str Operandos;
	auto Simbols = utilitarios::operadores();
	while(!Flujo.empty())
	{
		auto position=find(Simbols.begin(),Simbols.end(),Flujo.front());
		if(position!=Simbols.end())
		{
			_operacion->set_signo(*position);
            if(_operacion->get_Estado()==true && _operacion->get_lectura()!=trigonometrica)
                {
                    value Operador1,Operador2;
                    stringstream(Operandos.top()) >> Operador1;
                    Operandos.pop();
                    stringstream(Operandos.top()) >> Operador2;
                    Operandos.pop();
                    _operacion->evaluar(Operador1,Operador2);
                    _str numero = _operacion->getValor();
                    Operandos.push(numero);
                }
            else{
                   value Operador1;
                   value Operador2=0;
                    stringstream(Operandos.top()) >> Operador1;
                    this->puntos_x.push_back(Operador1);
                    this->puntos_y.push_back(Operador2);
                    Operandos.pop();
                    _operacion->evaluar(Operador1,Operador2);
                    _str numero = _operacion->getValor();
                    Operandos.push(numero);
            }
		}
		else
			Operandos.push(Flujo.front());
		Flujo.pop();
	}
	return Operandos.top();

}
vector_str tabulador::quitar_letras(_str& funcion){
        vector_str funciones;
        for(value i =x_negativo;i<x_positivo;i++){
         for(value k=0;k<101;k++){
            _str funcion_creada="";
            for(natural j=0;j<funcion.size();j++){
                natural valor = (int)funcion[j];
                if((valor==88 || valor==120)||(valor==89 || valor==121)){
                    funcion_creada=funcion_creada + Operaciones::to_str(i+(k/100));
                    this->puntos_x.push_back(i+(k/100));
                }
                else{
                    funcion_creada=funcion_creada + funcion[j];
                }
            }
            funciones.push_back(funcion_creada);
        }
    }
        return funciones;

}
void tabulador::tabular_funcion(_str& funcion){
     auto funciones = this->quitar_letras(funcion);
     for(auto it=funciones.begin();it<funciones.end();it++){
            _str funcion(*it);
            vector_str tokens= _parseador->tokenizer(funcion);
            cola_str valores = _parseador->shuntingYard(tokens);
            _str valor = this->evaluar(valores);
            value valor_insertar;
            stringstream(valor) >> valor_insertar;
            this->puntos_y.push_back(valor_insertar);
            cout<<"tabulando "<<funcion<<" = "<<valor<<endl;
     }
}
void tabulador::limpiar(){
   puntos_x.clear();
   puntos_y.clear();
}
