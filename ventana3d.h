#ifndef VENTANA3D_H
#define VENTANA3D_H

#include <QMainWindow>

#include <QMainWindow>
#include <QPushButton>
#include <QtDataVisualization>
#include <QFileDialog>
#include <QVector>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QList>
#include <QSignalMapper>
#include <string>
#include <QComboBox>
#include <Qstring>
#include <sstream>
#include "tabulador.h"

using namespace QtDataVisualization;
namespace Ui {
class ventana3d;
}

class ventana3d : public QMainWindow
{
    Q_OBJECT

public:
    explicit ventana3d(QWidget *parent = 0);
    ~ventana3d();
    _str quitarletra(_str& a,value i);
    void limpiar(natural);
    void graficar(_str& a,natural i,QColor color);
private slots:
    void on_regresar2d_clicked();
    void on_graficar_clicked();
    void on_limpiar_clicked();

    void on_graficar_2_clicked();

    void on_limpiar_2_clicked();

    void on_graficar_3_clicked();

    void on_limpiar_3_clicked();

    void on_salir_clicked();

    void on_guardar_clicked();

private:
    Ui::ventana3d *ui;
    Q3DSurface *m_graph;
        //QSurfaceDataProxy *m_sqrtSinProxy;
        //QSurface3DSeries *m_sqrtSinSeries;
     QList<QSurfaceDataProxy*>LDataProxy;
     QList<QSurface3DSeries*>L3DSeries;
     QMainWindow *_ventana;
     tabulador *_tabulador;
     QWidget *container;
     Q3DSurface *graph;


};

#endif // VENTANA3D_H
