#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tabulador.h"
#include "ventana3d.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_boton_graficar_clicked();
    void on_ir3d_clicked();

    void on_boton_limpiar_clicked();

private:
    Ui::MainWindow *ui;
    tabulador *_tabulador;
    ventana3d *_3d;
};

#endif // MAINWINDOW_H
