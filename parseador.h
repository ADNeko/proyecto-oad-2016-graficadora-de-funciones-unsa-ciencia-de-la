#ifndef PARSEADOR_H
#define PARSEADOR_H
#include "utilitarios.h"
#include "include.h"


class parseador
{
    public:
        parseador();
        vector_str tokenizer(_str & flujo);
        cola_str shuntingYard(vector_str& Tokens);
        virtual ~parseador();
    private:
};

#endif // PARSEADOR_H
