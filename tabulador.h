#ifndef TABULADOR_H
#define TABULADOR_H
#include "include.h"
#include "operacion.h"
#include "utilitarios.h"
#include "parseador.h"

class tabulador
{
    public:
        tabulador();
        tabulador(value,value);
        virtual ~tabulador();
        void tabular_funcion(_str& funcion);
        void tabular_funcion_3d(_str& funcion);
        vector_str quitar_letras(_str& funcion);
        vector_str quitar_letras_x(_str& funcion);
        vector_str quitar_letras_y(_str& funcion);
        _str evaluar(cola_str);
        _str evaluar_3d(cola_str);
        vector_value get_puntos_x(){return this->puntos_x;}
        vector_value get_puntos_y(){return this->puntos_y;}
        value get_x_pos(){return this->x_positivo;}
        value get_x_nega(){return this->x_negativo;}
        void limpiar();
        parseador *_parseador;
    private:
        Operacion *_operacion;
        value  x_positivo;
        value  x_negativo;
        vector_value puntos_x;
        vector_value puntos_y;

};

#endif // TABULADOR_H
