#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setWindowIcon(favicon);
    _tabulador= new tabulador(-25,25);
    _3d=new ventana3d;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete _tabulador;
    delete _3d;
    delete ui;
}


void MainWindow::on_boton_graficar_clicked()
{
    QString funcion = ui->funcion->text();
    _tabulador->limpiar();
    _str A(funcion.toStdString());
    _tabulador->tabular_funcion(A);
    vector_value puntos_x=_tabulador->get_puntos_x();
    vector_value puntos_y=_tabulador->get_puntos_y();
    vector_q_double x(1001), y(1001); // initialize with entries 0..100
    x.clear();
    y.clear();
    for (auto i=puntos_x.begin(); i<puntos_x.end(); ++i)
    {
      x.push_back(*i);
    }
    for (auto k=puntos_y.begin(); k<puntos_y.end(); ++k)
    {
      y.push_back(*k);
    }
    // create graph and assign data to it:
    ui->customPlot->addGraph();
    ui->customPlot->graph(0)->setData(x, y);
    // give the axes some labels:
    ui->customPlot->xAxis->setLabel("EJE x");
    ui->customPlot->yAxis->setLabel("EJE y");
    // set axes ranges, so we see all data:
    ui->customPlot->xAxis->setRange(-25, 25);
    ui->customPlot->yAxis->setRange(-25, 25);
    ui->customPlot->replot();
    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}



void MainWindow::on_ir3d_clicked()
{
    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
               if (widget->isHidden())
                   widget->show();
               }

       this->hide();
}

void MainWindow::on_boton_limpiar_clicked()
{
    _tabulador->limpiar();
}
