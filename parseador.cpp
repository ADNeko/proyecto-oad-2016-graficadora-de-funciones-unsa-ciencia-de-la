#include "parseador.h"

parseador::parseador()
{
    //ctor
}

parseador::~parseador()
{
    //dtor
}
vector_str parseador::tokenizer(_str & flujo)
{
    /*
    Dada una gramatica o tabla de simbolos, la siguiente porcion de la funcion tokenizer
    permitira discrimiar entre numero o simbolo y separara el flujo de carracteres en tokens que
    tengan un significado.
    */
    vector_str salida;
    //tabla de simbolos
    auto gram=utilitarios::gramatica();
    _str buffer("");
    _str key("");
    vector_str::iterator it2;
    for(auto it=flujo.begin();it!=flujo.end();it++)
    {
        key+=*it;
        it2=find(gram.begin(),gram.end(),key);
        if(it2==gram.end())
            buffer+=*it;
        else
        {
            salida.push_back(buffer);
            buffer="";
            if((*it2).size()==1)
                salida.push_back(*it2);
        }
        key="";
    }
    if(buffer!="")
        salida.push_back(buffer);
    /*
    Esta seccion se encargara de eliminar los tokens muertos o ""
    */
    vector_str::iterator seekerSpace;
    string space="";
    seekerSpace=find(salida.begin(),salida.end(),space);
    while(seekerSpace!=salida.end())
    {
        salida.erase(seekerSpace);
        seekerSpace=find(salida.begin(),salida.end(),space);
    }
    /*
    Esta seccion del codigo analiza cada token procesado, si hay un "-" asociado a un numero los juntar como
    un \'unico token.
    */
    vector_str temp;
    vector_str::iterator iterador=salida.begin();
    while(iterador!=salida.end())
    {
        _str fuzor("");
        vector_str::iterator tmpIzquierda,tmpDerecha,previus,next;
        previus=iterador;
        next=iterador;
        if(previus!=salida.begin())
            previus--;
        next++;
        if(next==salida.end())
            next--;
        tmpIzquierda=find(gram.begin(),gram.end(),*previus);
        tmpDerecha=find(gram.begin(),gram.end(),*next);
        if(*iterador=="-")
        {
            if(tmpIzquierda!=gram.end())
            {
                fuzor+=*iterador;
                fuzor+=*next;
                temp.push_back(fuzor);
                iterador++;
                iterador++;
            }
        }
        if(iterador!=salida.end())
        {
            temp.push_back(*iterador);
            iterador++;
        }
    }
    salida=temp;
    return salida;
}
/*El algoritmo shuntingYard yard, recibe un flujo de tokens en este caso almacenados en un vector como strings*/
cola_str parseador::shuntingYard(vector_str& Tokens)
{
/*Establecemos el rango de prioridades de cada operacin, los de numeros msaltos se debn de resolver primero*/
    auto  Tabla=utilitarios::tabla_prioriades();
    cola_str salida;
    stack_str operadores;
    str_natural::iterator sentinel;
    str_natural::iterator sentinel2;
    sentinel2=Tabla.find("?");//se inicializa a sentinel2 con un valor muy alto
    /*Analizamos el flujo*/
    for(auto it=Tokens.begin();it!=Tokens.end();it++)
    {
        /*Si el token no fue encontrado en la tabla de simbolos, hay 2 posibilidades, es un numero o una variable, asi que lo ponemos en la salida "Cola"*/
        sentinel=Tabla.find(*it);
        if(sentinel==Tabla.end())
        {
            salida.push(*it);
            //cout<<"Token Numero o variable agregado"<<endl;
        }
        else
        {
        /*En caso de que si se encuentre en la tabla de simbolos, analizaremos su prioridad, y usaremos una pila para alamacenar temporalmente
        los operadores que se encuentren en el flujo
            */
            if((*sentinel).first!="("&&(*sentinel).first!=")")
            {
        /*En caso exista un operador en la pila y exista un operador con mayor precedencia que el que heencontrado, ponemos el operador con alta precedencia
        en la salida cola,
            */
                while(!operadores.empty()&&(*sentinel).second<(*sentinel2).second&&operadores.top()!="(")
                {
                    salida.push(operadores.top());
                    operadores.pop();
                }
        /*En su defecto ponemos en la pila de operadores */
                operadores.push(*it);
                sentinel2=Tabla.find(*it);
            }
        /*Si es un parentesis, lo ponemos izquierdo, lo ponemos temporalmente a la pila de operadores*/
            if((*sentinel).first=="(")
            {
                operadores.push(*it);
            }
        /*En caso sea un parentesis derecho, extraemos todos los operadores en la pila hacia la cola de salida, hasta que encontremos un parentesis izquierdo
        en la pila y ser descartado de la salida	*/
            if((*sentinel).first==")")
            {
                //while(operadores.top()!="(")
                while(operadores.top()!="(")
                {
                    salida.push(operadores.top());
                    operadores.pop();
                }
                operadores.pop();
                if(!operadores.empty())
                {
                    salida.push(operadores.top());
                    operadores.pop();
                }
            }
        }
    }
    /*Una vez procesado todo el flujo, extraemos todos los operadores que quedaron en lapilahacia la cola*/
    while(!operadores.empty())
    {
        salida.push(operadores.top());
        operadores.pop();
    }
    /*Retornamos una cola con el flujo en notacin postfija*/
    return salida;
}
