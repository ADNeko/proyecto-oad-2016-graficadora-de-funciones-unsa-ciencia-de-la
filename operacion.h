#ifndef OPERACION_H
#define OPERACION_H
#include "include.h"
#include "operaciones.h"

using namespace std;



class Operacion
{
public:
            /** \brief Constructor por defecto
             *
             * \param simbolo de la funcion a evaluar
             */
            Operacion();
            bool get_Estado(){return existe_operacion;}
            _str get_lectura(){return operacion_lectura;}
            void set_signo(const _str&);
            _str getSet_signo(){return signo;}
            void evaluar(value &a, value &b);
            _str getValor(){return valor;}
            virtual ~Operacion();

private:
            /**< Mienbro dato , Puntero a funciones  */
            ptr_funcion m_ptr_funcion;
            logico existe_operacion;
            str_funcion Tabla_operaciones;
            str_str lectura_operadores;
            _str operacion_lectura;
            _str signo;
           _str valor;

};

#endif // OPERACION_H
