#ifndef INLCUDE_H
#define INCLUDE_H
#include "string"
#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <QMainWindow>
#include <QIcon>
#include <QPixmap>
#define derecha_izquierda  "derecha-izquierda"
#define izquierda_derecha  "izquierda-derecha"
#define trigonometrica "trigonometrica"

using namespace  std ;
/** variable a usar en tipos strings**/
typedef string cadena;
typedef string _str;
/** variable a usar en tipos float**/
typedef double value;
/** variable a usar en tipos double**/
typedef double valor;
/** variable a usar en tipos char**/
typedef char caracter;
/** variable a usar en tipos int**/
typedef int posicion;
typedef int natural;
typedef int prioridad;
/** variable a usar en tipos booleano**/
typedef bool logico;
/** variable a usar en tipos vector string**/
typedef vector<_str> vector_str;
/** variable a usar en tipos vector doubles**/
typedef vector<value> vector_value;
/** variable a usar en tipos cola string**/
typedef queue<_str> cola_str;
/** variable a usar en tipos stack string**/
typedef stack<_str> stack_str;
/** variable a usar en punteros a funcion float**/
typedef value(*ptr_funcion)(value,value);
/** variable a usar en tipos maop string-Entero**/
typedef map<_str,natural> str_natural;
/** variable a usar en tipos maop string-a puntero funcion**/
typedef map<_str,ptr_funcion> str_funcion;
/** variable a usar en tipos maop string-string**/
typedef map<_str,_str>  str_str;

typedef QVector<double> vector_q_double;

#define favicon QPixmap(":/image/descarga.png")

using namespace std;




#endif // INCLUDE_H
