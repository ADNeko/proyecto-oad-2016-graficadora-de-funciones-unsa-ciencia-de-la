#include "ventana3d.h"
#include "ui_ventana3d.h"

ventana3d::ventana3d(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ventana3d)
{
    ui->setupUi(this);
    //Contenedor para el 3D
    this->setWindowIcon(favicon);
       graph = new Q3DSurface();
       container = QWidget::createWindowContainer(graph,this);
       container->move(540,60);
       container->setMinimumWidth(701);
       container->setMinimumHeight(511);

       container->show();

       m_graph=graph;
       m_graph->activeTheme()->setType(Q3DTheme::ThemeStoneMoss);

       //

       _tabulador = new tabulador(-25,25);
       m_graph->axisX()->setRange(-50,50);
       m_graph->axisY()->setRange(-50,50);
       m_graph->axisZ()->setRange(-50,50);
       m_graph->axisX()->setLabelAutoRotation(30);
       m_graph->axisY()->setLabelAutoRotation(90);
       m_graph->axisZ()->setLabelAutoRotation(30);
       m_graph->axisX()->setTitle(QStringLiteral("Eje X"));
       m_graph->axisY()->setTitle(QStringLiteral("Eje Y"));
       m_graph->axisZ()->setTitle(QStringLiteral("Eje Z"));
       m_graph->axisX()->setTitleVisible(true);
       m_graph->axisY()->setTitleVisible(true);
       m_graph->axisZ()->setTitleVisible(true);

}

ventana3d::~ventana3d()
{
    delete _tabulador;
    delete ui;
}

void ventana3d::on_regresar2d_clicked()
{
    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
               if (widget->isHidden())
                   widget->show();
               }

    this->hide();
}
_str ventana3d::quitarletra(_str& funcion,value i){
    _str funcion_creada="";
    logico bandera;
    natural letra;
    for(natural k=0;k<funcion.size();k++){
        natural valor = (int)funcion[k];
        if((valor==88 || valor==120)||(valor==89 || valor==121)){
            if(bandera==false){
                letra=valor;
                bandera=true;
            }
            if(letra==valor){

                  funcion_creada=funcion_creada + Operaciones::to_str(i);
            }
            else{
                funcion_creada=funcion_creada + funcion[k];
            }
        }
        else{
            funcion_creada=funcion_creada + funcion[k];
        }
    }
        return funcion_creada;
}
void ventana3d::on_limpiar_clicked()
{
    limpiar(0);
}
void ventana3d::graficar(_str& a,natural i,QColor color){

    QSurfaceDataProxy *Dataproxy = new QSurfaceDataProxy;
    LDataProxy.insert(i,Dataproxy);
    QSurface3DSeries *Series = new QSurface3DSeries(LDataProxy[i]);
    L3DSeries.insert(i,Series);

    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
    dataArray->reserve(150);
    for(value i=-25;i<25;i++){
             _str buffer=this->quitarletra(a,i);
             QSurfaceDataRow *newRow = new QSurfaceDataRow(150);
             // Keep values within range bounds, since just adding step can cause minor drift due
             // to the rounding errors.
             int index = 0;
             for(value k=-25;k<25;k++){
               _str buffer2=this->quitarletra(buffer,k);
                _str funcion(buffer2);
               vector_str tokens= _tabulador->_parseador->tokenizer(funcion);
               cola_str valores = _tabulador->_parseador->shuntingYard(tokens);
               _str valor = _tabulador->evaluar(valores);
               value valor_insertar;
               stringstream(valor) >> valor_insertar;
               if(valor_insertar>50){
                   valor_insertar=49.9;
               }
               if(valor_insertar<-50){
                   valor_insertar=-49.9;
               }
               cout<<i<<"-"<<k<<"-"<<valor_insertar<<endl;
              (*newRow)[index++].setPosition(QVector3D(i,k,valor_insertar));
             }
             *dataArray << newRow;
        }
        LDataProxy[i]->resetArray(dataArray);
        L3DSeries[i]->setDrawMode(QSurface3DSeries::DrawSurface);
        L3DSeries[i]->setFlatShadingEnabled(true);
        L3DSeries[i]->setBaseColor(color);
        m_graph->addSeries(L3DSeries[i]);
}

void ventana3d::on_graficar_clicked()
{
    QString funcion1=ui->text_funcion->text();
    string A(funcion1.toStdString());
    graficar(A,0,QColor(Qt::green));
}

void ventana3d::on_graficar_2_clicked()
{
    QString funcion1=ui->text_funcion_2->text();
    string A(funcion1.toStdString());
    graficar(A,1,QColor(Qt::red));
}
void ventana3d::limpiar(natural i){
    m_graph->removeSeries(L3DSeries[i]);
    LDataProxy.removeAt(i);
    L3DSeries.removeAt(i);
}


void ventana3d::on_limpiar_2_clicked()
{
    limpiar(1);
}

void ventana3d::on_graficar_3_clicked()
{
    QString funcion1=ui->text_funcion_3->text();
    string A(funcion1.toStdString());
    graficar(A,2,QColor(Qt::blue));
}

void ventana3d::on_limpiar_3_clicked()
{
    limpiar(2);
}

void ventana3d::on_salir_clicked()
{
    this->close();
}

void ventana3d::on_guardar_clicked()
{
   QString name = ui->namepng->text();
   graph->renderToImage().save(name);
}
