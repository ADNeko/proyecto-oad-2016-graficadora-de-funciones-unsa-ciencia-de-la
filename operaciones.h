﻿#ifndef OPERACIONES_H
#define OPERACIONES_H
#include <math.h>
#include <map>
#include "include.h"
#define PI 3.14159265

/**inline: Esta indica al compilador que cada llamado a la función inline deberá ser reemplazado por el cuerpo de esta función.**/
namespace Operaciones
{
    /** \brief Realiza la suma entre dos valores
     *
     * \param a primer parametro
     * \param  b segundo parametro
     * \return suma entre a y b
     *
     */
    inline value adicion(value a, value b){return a+b;}

  /** \brief Realiza la resta entre dos valores
     *
     * \param a primer parametro
     * \param  b segundo parametro
     * \return resta  entre a y b
     */
  inline value sustraccion(value a,value b){return a-b;}


   /** \brief Realiza la divison  entre dos valores
     *
     * \param a primer parametro
     * \param  b segundo parametro
     * \return divison  entre a y b
     *
     */
  inline value division(value a,value b){return a/b;}

   /** \brief Realiza la multiplicacion entre dos valores
     *
     * \param a primer parametro
     * \param  b segundo parametro
     * \return multiplica entre a y b
     *
     */
  inline value multiplicacion(value a,value b){return (b==0)?0:a*b;}

/** \brief Realiza la potencia  entre dos valores
     *
     * \param a primer parametro
     * \param  b segundo parametro
     * \return potencia  entre a y b
     *
     */
  inline  value potencia(const value a,const value  b){return pow(a,b);}
  
  inline  value seno(const value a,const value b){return sin(a);}
  
  inline  value coseno(const value a,const value b){return cos(a);}
  
  template <typename T>
    
 _str to_str(const T& t) {
           
		ostringstream os;
           
		os<<t;
           
		return os.str();
    
 }

};

#endif // OPERACIONES_H
