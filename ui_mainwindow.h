/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *ir3d;
    QCustomPlot *customPlot;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QComboBox *comboxSenCos_2;
    QComboBox *comboBox_4;
    QComboBox *comboBox_5;
    QPushButton *divide_2;
    QPushButton *n_10;
    QPushButton *n_11;
    QPushButton *n_12;
    QPushButton *n_13;
    QPushButton *n_14;
    QPushButton *multiplica_2;
    QPushButton *n_15;
    QPushButton *enter_2;
    QPushButton *igual_2;
    QPushButton *n_16;
    QPushButton *resta_2;
    QPushButton *suma_2;
    QPushButton *punto_2;
    QPushButton *n_17;
    QPushButton *n_18;
    QPushButton *n_19;
    QLineEdit *lineEdit_2;
    QPushButton *boton_guardar;
    QLineEdit *funcion;
    QPushButton *boton_graficar;
    QPushButton *boton_limpiar;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(994, 663);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        ir3d = new QPushButton(centralWidget);
        ir3d->setObjectName(QStringLiteral("ir3d"));
        ir3d->setGeometry(QRect(820, 520, 99, 27));
        QFont font;
        font.setFamily(QStringLiteral("OpineHeavy"));
        ir3d->setFont(font);
        customPlot = new QCustomPlot(centralWidget);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        customPlot->setGeometry(QRect(30, 110, 501, 361));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(600, 300, 360, 167));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        comboxSenCos_2 = new QComboBox(layoutWidget);
        comboxSenCos_2->setObjectName(QStringLiteral("comboxSenCos_2"));

        gridLayout_2->addWidget(comboxSenCos_2, 0, 0, 1, 1);

        comboBox_4 = new QComboBox(layoutWidget);
        comboBox_4->setObjectName(QStringLiteral("comboBox_4"));

        gridLayout_2->addWidget(comboBox_4, 0, 2, 1, 1);

        comboBox_5 = new QComboBox(layoutWidget);
        comboBox_5->setObjectName(QStringLiteral("comboBox_5"));

        gridLayout_2->addWidget(comboBox_5, 0, 1, 1, 1);

        divide_2 = new QPushButton(layoutWidget);
        divide_2->setObjectName(QStringLiteral("divide_2"));

        gridLayout_2->addWidget(divide_2, 0, 3, 1, 1);

        n_10 = new QPushButton(layoutWidget);
        n_10->setObjectName(QStringLiteral("n_10"));

        gridLayout_2->addWidget(n_10, 1, 0, 1, 1);

        n_11 = new QPushButton(layoutWidget);
        n_11->setObjectName(QStringLiteral("n_11"));

        gridLayout_2->addWidget(n_11, 2, 0, 1, 1);

        n_12 = new QPushButton(layoutWidget);
        n_12->setObjectName(QStringLiteral("n_12"));

        gridLayout_2->addWidget(n_12, 2, 1, 1, 1);

        n_13 = new QPushButton(layoutWidget);
        n_13->setObjectName(QStringLiteral("n_13"));

        gridLayout_2->addWidget(n_13, 1, 1, 1, 1);

        n_14 = new QPushButton(layoutWidget);
        n_14->setObjectName(QStringLiteral("n_14"));

        gridLayout_2->addWidget(n_14, 1, 2, 1, 1);

        multiplica_2 = new QPushButton(layoutWidget);
        multiplica_2->setObjectName(QStringLiteral("multiplica_2"));

        gridLayout_2->addWidget(multiplica_2, 1, 3, 1, 1);

        n_15 = new QPushButton(layoutWidget);
        n_15->setObjectName(QStringLiteral("n_15"));

        gridLayout_2->addWidget(n_15, 2, 2, 1, 1);

        enter_2 = new QPushButton(layoutWidget);
        enter_2->setObjectName(QStringLiteral("enter_2"));

        gridLayout_2->addWidget(enter_2, 4, 3, 1, 1);

        igual_2 = new QPushButton(layoutWidget);
        igual_2->setObjectName(QStringLiteral("igual_2"));

        gridLayout_2->addWidget(igual_2, 4, 2, 1, 1);

        n_16 = new QPushButton(layoutWidget);
        n_16->setObjectName(QStringLiteral("n_16"));

        gridLayout_2->addWidget(n_16, 4, 0, 1, 1);

        resta_2 = new QPushButton(layoutWidget);
        resta_2->setObjectName(QStringLiteral("resta_2"));

        gridLayout_2->addWidget(resta_2, 2, 3, 1, 1);

        suma_2 = new QPushButton(layoutWidget);
        suma_2->setObjectName(QStringLiteral("suma_2"));

        gridLayout_2->addWidget(suma_2, 3, 3, 1, 1);

        punto_2 = new QPushButton(layoutWidget);
        punto_2->setObjectName(QStringLiteral("punto_2"));

        gridLayout_2->addWidget(punto_2, 4, 1, 1, 1);

        n_17 = new QPushButton(layoutWidget);
        n_17->setObjectName(QStringLiteral("n_17"));

        gridLayout_2->addWidget(n_17, 3, 0, 1, 1);

        n_18 = new QPushButton(layoutWidget);
        n_18->setObjectName(QStringLiteral("n_18"));

        gridLayout_2->addWidget(n_18, 3, 1, 1, 1);

        n_19 = new QPushButton(layoutWidget);
        n_19->setObjectName(QStringLiteral("n_19"));

        gridLayout_2->addWidget(n_19, 3, 2, 1, 1);

        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(640, 190, 311, 31));
        boton_guardar = new QPushButton(centralWidget);
        boton_guardar->setObjectName(QStringLiteral("boton_guardar"));
        boton_guardar->setGeometry(QRect(640, 230, 311, 31));
        boton_guardar->setFont(font);
        funcion = new QLineEdit(centralWidget);
        funcion->setObjectName(QStringLiteral("funcion"));
        funcion->setGeometry(QRect(640, 110, 311, 31));
        boton_graficar = new QPushButton(centralWidget);
        boton_graficar->setObjectName(QStringLiteral("boton_graficar"));
        boton_graficar->setGeometry(QRect(640, 150, 131, 31));
        boton_graficar->setFont(font);
        boton_limpiar = new QPushButton(centralWidget);
        boton_limpiar->setObjectName(QStringLiteral("boton_limpiar"));
        boton_limpiar->setGeometry(QRect(820, 150, 131, 31));
        boton_limpiar->setFont(font);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(600, 50, 351, 20));
        QFont font1;
        font1.setFamily(QStringLiteral("Showcard Gothic"));
        font1.setPointSize(16);
        label_3->setFont(font1);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(110, 60, 351, 20));
        label_4->setFont(font1);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(580, 120, 47, 13));
        QFont font2;
        font2.setFamily(QStringLiteral("November"));
        font2.setPointSize(10);
        label_5->setFont(font2);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(580, 200, 47, 13));
        label_2->setFont(font2);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 994, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "GRAFICADORA DE FUNCIONES", 0));
        ir3d->setText(QApplication::translate("MainWindow", "Ir a 3d", 0));
        comboxSenCos_2->clear();
        comboxSenCos_2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "sen", 0)
         << QApplication::translate("MainWindow", "cos", 0)
        );
        comboBox_4->clear();
        comboBox_4->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "sec", 0)
         << QApplication::translate("MainWindow", "csc", 0)
        );
        comboBox_5->clear();
        comboBox_5->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "tan", 0)
         << QApplication::translate("MainWindow", "cot", 0)
        );
        divide_2->setText(QApplication::translate("MainWindow", "/", 0));
        n_10->setText(QApplication::translate("MainWindow", "7", 0));
        n_11->setText(QApplication::translate("MainWindow", "4", 0));
        n_12->setText(QApplication::translate("MainWindow", "5", 0));
        n_13->setText(QApplication::translate("MainWindow", "8", 0));
        n_14->setText(QApplication::translate("MainWindow", "9", 0));
        multiplica_2->setText(QApplication::translate("MainWindow", "X", 0));
        n_15->setText(QApplication::translate("MainWindow", "6", 0));
        enter_2->setText(QApplication::translate("MainWindow", "Enter", 0));
        igual_2->setText(QApplication::translate("MainWindow", "=", 0));
        n_16->setText(QApplication::translate("MainWindow", "0", 0));
        resta_2->setText(QApplication::translate("MainWindow", "-", 0));
        suma_2->setText(QApplication::translate("MainWindow", "+", 0));
        punto_2->setText(QApplication::translate("MainWindow", ".", 0));
        n_17->setText(QApplication::translate("MainWindow", "1", 0));
        n_18->setText(QApplication::translate("MainWindow", "2", 0));
        n_19->setText(QApplication::translate("MainWindow", "3", 0));
        boton_guardar->setText(QApplication::translate("MainWindow", "GUARDAR", 0));
        boton_graficar->setText(QApplication::translate("MainWindow", "GRAFICAR", 0));
        boton_limpiar->setText(QApplication::translate("MainWindow", "LIMPIAR", 0));
        label_3->setText(QApplication::translate("MainWindow", "PANEL DE CONTROL FUNCIONES 2D", 0));
        label_4->setText(QApplication::translate("MainWindow", "GRAFICA DE LA FUNCI\303\223N", 0));
        label_5->setText(QApplication::translate("MainWindow", "F(x) =", 0));
        label_2->setText(QApplication::translate("MainWindow", "Nombre", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
